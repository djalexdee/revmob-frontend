import React, { Component } from 'react';
import qs from 'querystring';
import api from '../services/api';
import CampaignTable from '../components/table/CampaignTable';
import AddCampaignForm from '../components/forms/AddCampaignForm';

class Home extends Component {

    constructor(props) {
        super(props);

        this.state = {
            campaigns: [],
            currentCampaign: { id: null, conversionType: '', countryTargeting: '' },
            editing: false
        }
    }

    componentDidMount() {
        this.refreshCampaignTable();
    }

    refreshCampaignTable() {
        this.campaignData = api.get('campaigns/getCampaigns')
            .then(response => response.data)
            .then(data => {

                this.setState({ 
                    campaigns: data.data,
                    setCampaigns: data.data
                });
            });
    }

    addCampaign = campaign => {

        api.post('campaigns/createCampaign', {conversionType:campaign.conversionType, countryTargeting: campaign.countryTargeting})
            .then(res => {
                this.refreshCampaignTable();
            });
    };

    render () {
        const { campaigns } = this.state;

        return (
            <div className="container">
                    
                <div className="row">
                        <div className="col s12 l6">
                            <h4>Add Campaign</h4>
                            <AddCampaignForm addCampaign={this.addCampaign} />
                        </div>      
                    <div className="col s12 l6">
                        <h5>Campaigns</h5>
                        <CampaignTable campaigns={campaigns} editRow={this.editRow} deleteUser={this.deleteUser} />
                    </div>
                </div>
            </div>
        );
    };
};

export default Home;