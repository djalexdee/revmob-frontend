import React, { useState } from 'react';

const AddCampaignForm = props => {

    const initialFormState = { conversionType: '', countryTargeting: ''};
    const [campaign, setCampaign] = useState(initialFormState);

    const handleInputChange = event => {
        const {name, value} = event.target;

        setCampaign({ ...campaign, [name]: value });
    }

    const submitForm = event => {
        event.preventDefault();

        if (!campaign.conversionType || !campaign.countryTargeting) return;

        props.addCampaign(campaign);
        setCampaign(initialFormState);
    };

    return (
        <div className="row">

            <form className="col s12"
                onSubmit={submitForm}>
                <div className="row">
                    <div className="input-field col s12">

                        <input type="text" 
                            id="conversionType" 
                            name="conversionType" 
                            value={campaign.conversionType}
                            onChange={handleInputChange} 
                            required />
                        <label htmlFor="conversionType">Converion Type</label>
                    </div>
                </div>

                <div className="row">
                    <div className="input-field col s12">

                        <input 
                            type="text" 
                            name="countryTargeting" 
                            value={campaign.countryTargeting}
                            onChange={handleInputChange} 
                            required />
                        <label htmlFor="">Country Targeting</label>
                    </div>
                </div>
                
                <div className="row">
                    <div className="input-field col s12">

                        <button className="waves-effect waves-light btn">Add</button>
                    </div>
                </div>
            </form>
        </div>
    );
};

export default AddCampaignForm;
