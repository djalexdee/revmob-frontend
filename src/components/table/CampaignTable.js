import React from 'react';

const CampaignTable = props => (
  
    <table className="responsive-table">
        <thead>
            <tr>
                <th>Conversion Type</th>
                <th>Country Targeting</th>
            </tr>
        </thead>
    <tbody>
        {
            props.campaigns.length > 0 ? (
                props.campaigns.map (campaign => (

                    <tr key={campaign.id}>
                        <td>{campaign.conversionType}</td>
                        <td>{campaign.countryTargeting}</td>
                    </tr>
                    ))
                ) : (
                    <tr>
                        <td colSpan={3}>{props.campaigns[0]} No Campaigns</td>
                    </tr>
                )
        }          
    </tbody>
  </table>
);
    
export default CampaignTable;